import {select, classNames, templates} from '../settings.js';
import utils from '../utils.js';
import AmountWidget from './AmountWidget.js';

export default class Product {
  constructor( id, data ){
    const thisProduct = this;

    thisProduct.id = id;
    thisProduct.data = data;

    thisProduct.renderInMenu();
    thisProduct.getElements();
    thisProduct.initAccordion();
    thisProduct.initOrderForm();
    thisProduct.initAmountWidget();
    thisProduct.processOrder();

    // console.log('New product: ', thisProduct);
  }

  renderInMenu(){
    const thisProduct = this;

    /* generate HTML based on template */
    const generatedHTML = templates.menuProduct( thisProduct.data );
    // console.log( generatedHTML );

    /* create element using utils.createElementFromHTML */
    thisProduct.element = utils.createDOMFromHTML( generatedHTML );

    /* find menu container */
    const menuContainer = document.querySelector(select.containerOf.menu);

    /* add element to menu */
    menuContainer.appendChild( thisProduct.element );
  }
  initAccordion(){
    const thisProduct = this;

    /* find the clickable trigger (the element that should react to clicking) */
    const productHeader = thisProduct.accordionTrigger;

    /* START: click event listener to trigger */
    productHeader.addEventListener('click', function(event){
      /* prevent default action for event */
      event.preventDefault();

      /* toggle active class on element of thisProduct */
      thisProduct.element.classList.toggle( classNames.menuProduct.wrapperActive );

      /* find all active products */
      const allActiveProducts = document.querySelectorAll( select.all.menuProductsActive );

      /* START LOOP: for each active product */
      for ( let product of allActiveProducts) {
        /* START: if the active product isn't the element of thisProduct */
        if ( product !== thisProduct.element ) {
          /* remove class active for the active product */
          product.classList.remove( classNames.menuProduct.wrapperActive );
        } /* END: if the active product isn't the element of thisProduct */
      } /* END LOOP: for each active product */
    });/* END: click event listener to trigger */
  }
  initOrderForm(){
    const thisProduct = this;
    // console.log('initOrderForm');

    thisProduct.form.addEventListener('submit', function(event){
      event.preventDefault();
      thisProduct.processOrder();
    });

    for(let input of thisProduct.formInputs){
      input.addEventListener('change', function(){
        thisProduct.processOrder();
      });
    }

    thisProduct.cartButton.addEventListener('click', function(event){
      event.preventDefault();
      thisProduct.processOrder();
      thisProduct.addToCart();
    });
  }
  processOrder(){
    const thisProduct = this;
    const formData = utils.serializeFormToObject(thisProduct.form);
    const productParams = thisProduct.data.params;
    let productPrice = thisProduct.data.price;

    thisProduct.params = {};

    /*
    console.log({
      formData: formData,
      thisProduct: thisProduct
    });
    */
    /* Loop through product params */
    for (const param in productParams) {
      const paramOptions = productParams[param]['options'];

      // Loop through param options
      for (const option in paramOptions) {
        const paramOption = paramOptions[option];
        // Check if formData has the property of the param name
        const optionSelected = formData.hasOwnProperty(param) && formData[param].indexOf(option) > -1;
        // console.log(option, optionSelected, paramOption.default);
        // Check if form array includes an item with the option name - if the option is selected
        if ( optionSelected && !paramOption.default) {
          // If the options parameter 'default' is set to false add to price
          // calculatedPrice = productPrice + paramOption.price;
          productPrice += paramOption.price;
          // If the options parameter 'default' is set to true deduct from price
        } else if ( !optionSelected && paramOption.default ) {
          // calculatedPrice = productPrice - paramOption.price;
          productPrice -= paramOption.price;
        }

        const imagesSelector = '.' + param + '-' + option;
        const optionImages = thisProduct.imageWrapper.querySelectorAll(imagesSelector);

        for ( let img of optionImages ) {
          if ( optionSelected ) {
            img.classList.add(classNames.menuProduct.imageVisible);
          } else {
            img.classList.remove(classNames.menuProduct.imageVisible);
          }
        }
        if ( optionSelected ) {
          if( !thisProduct.params[param] ) {
            thisProduct.params[param] = {
              label: productParams[param].label,
              options: {},
            };
          }
          thisProduct.params[param].options[option] = paramOptions[option].label;
        }
      }
    }

    /* multiply price by amount */
    thisProduct.priceSingle = productPrice;
    thisProduct.price = thisProduct.priceSingle * thisProduct.amountWidget.value;

    /* set the contents of thisProduct.priceElem to be the value of variable price */
    thisProduct.priceElem.innerHTML = thisProduct.price;
  }
  getElements(){
    const thisProduct = this;

    thisProduct.accordionTrigger = thisProduct.element.querySelector(select.menuProduct.clickable);
    thisProduct.form = thisProduct.element.querySelector(select.menuProduct.form);
    thisProduct.formInputs = thisProduct.form.querySelectorAll(select.all.formInputs);
    thisProduct.cartButton = thisProduct.element.querySelector(select.menuProduct.cartButton);
    thisProduct.priceElem = thisProduct.element.querySelector(select.menuProduct.priceElem);
    thisProduct.imageWrapper = thisProduct.element.querySelector(select.menuProduct.imageWrapper);
    thisProduct.amountWidgetElem = thisProduct.element.querySelector(select.menuProduct.amountWidget);
  }
  initAmountWidget(){
    const thisProduct = this;

    thisProduct.amountWidget = new AmountWidget( thisProduct.amountWidgetElem );
    thisProduct.amountWidgetElem.addEventListener('updated', function(){
      thisProduct.processOrder();
    });
  }
  addToCart(){
    const thisProduct = this;
    thisProduct.name = thisProduct.data.name;
    thisProduct.amount = thisProduct.amountWidget.value;
    // app.cart.add(thisProduct);
    const event = new CustomEvent('add-to-cart', {
      bubbles: true,
      detail: {
        product: thisProduct,
      },
    });
    thisProduct.element.dispatchEvent(event);
  }
}
