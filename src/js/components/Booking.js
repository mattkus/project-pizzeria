import {select, templates, settings, classNames} from '../settings.js';
import utils from '../utils.js';
import AmountWidget from './AmountWidget.js';
import DatePicker from './DatePicker.js';
import HourPicker from './HourPicker.js';

export default class Booking {
  constructor( element ){
    const thisBooking = this;
    thisBooking.render( element );
    thisBooking.initWidgets();
    thisBooking.initActions();
    thisBooking.getData();
  }
  render( element ){
    const thisBooking = this;
    const generatedHTML = templates.bookingWidget();
    const generatedDOM = utils.createDOMFromHTML( generatedHTML );

    thisBooking.dom = {};
    thisBooking.dom.wrapper = element;
    thisBooking.dom.wrapper.appendChild(generatedDOM);
    thisBooking.dom.peopleAmount = thisBooking.dom.wrapper.querySelector(select.booking.peopleAmount);
    thisBooking.dom.hoursAmount = thisBooking.dom.wrapper.querySelector(select.booking.hoursAmount);
    thisBooking.dom.datePicker = thisBooking.dom.wrapper.querySelector(select.widgets.datePicker.wrapper);
    thisBooking.dom.hourPicker = thisBooking.dom.wrapper.querySelector(select.widgets.hourPicker.wrapper);
    thisBooking.dom.tables = thisBooking.dom.wrapper.querySelectorAll(select.booking.tables);
    thisBooking.dom.form = thisBooking.dom.wrapper.querySelector(select.booking.form);
  }
  initActions(){
    const thisBooking = this;

    thisBooking.dom.wrapper.addEventListener('updated', thisBooking.updateDOM.bind(thisBooking));

    for ( let table of thisBooking.dom.tables) {
      table.addEventListener('click', function(){
        thisBooking.toggleTableSelection(table);
      });
    }

    thisBooking.dom.form.addEventListener('submit', function(e){
      e.preventDefault();
      thisBooking.makeBooking();
    });
  }
  initWidgets(){
    const thisBooking = this;

    thisBooking.peopleAmount = new AmountWidget( thisBooking.dom.peopleAmount );
    thisBooking.hoursAmount = new AmountWidget( thisBooking.dom.hoursAmount );
    thisBooking.datePicker = new DatePicker( thisBooking.dom.datePicker );
    thisBooking.hourPicker = new HourPicker( thisBooking.dom.hourPicker );
    // console.log(thisBooking.peopleAmount);
  }
  getData(){
    const thisBooking = this;

    const startEndDates = {};
    startEndDates[settings.db.dateStartParamKey] = utils.dateToStr( new Date(thisBooking.datePicker.minDate) );
    startEndDates[settings.db.dateEndParamKey] = utils.dateToStr( new Date(thisBooking.datePicker.maxDate) );

    const endDate = {};
    endDate[settings.db.dateEndParamKey] = startEndDates[settings.db.dateEndParamKey];

    const params = {
      booking: utils.queryParams(startEndDates),
      eventsCurrent: settings.db.notRepeatParam + '&' + utils.queryParams(startEndDates),
      eventsRepeat: settings.db.repeatParam + '&' + utils.queryParams(endDate),
    };
    // console.log('getData params', params);

    const urls = {
      booking: settings.db.url + '/' + settings.db.booking + '?' + params.booking,
      eventsCurrent: settings.db.url + '/' + settings.db.event + '?' + params.eventsCurrent,
      eventsRepeat: settings.db.url + '/' + settings.db.event + '?' + params.eventsRepeat,
    };
    // console.log('getData urls', urls);

    Promise.all([
      fetch(urls.booking),
      fetch(urls.eventsCurrent),
      fetch(urls.eventsRepeat),
    ])
      .then(function([bookingsResponse, eventsCurrentResponse, eventsRepeatResponse]){
        return Promise.all([
          bookingsResponse.json(),
          eventsCurrentResponse.json(),
          eventsRepeatResponse.json(),
        ]);
      })
      .then(function([bookings, eventsCurrent, eventsRepeat]){
        thisBooking.parseData(bookings, eventsCurrent, eventsRepeat);
      });
  }
  parseData( bookings, eventsCurrent, eventsRepeat ){
    const thisBooking = this;
    thisBooking.booked = {};

    // console.log(eventsCurrent);
    for ( let event of eventsCurrent ) {
      // console.log(event);
      thisBooking.makeBooked(
        event.date,
        event.hour,
        event.table,
        event.duration
      );
    }
    for ( let event of bookings ) {
      // console.log('Booking event: ', event);
      thisBooking.makeBooked(
        event.date,
        event.hour,
        event.table,
        event.duration
      );
    }
    for ( let event of eventsRepeat ) {
      if ( event.repeat == 'daily' ) {
        for ( let date = thisBooking.datePicker.minDate; date <= thisBooking.datePicker.maxDate; date = utils.addDays(date, 1) ) {
          thisBooking.makeBooked(
            utils.dateToStr(date),
            event.hour,
            event.table,
            event.duration
          );
        }
      }
    }

    // console.log(thisBooking.booked);
    thisBooking.updateDOM();
  }
  makeBooked(date, hour, table, duration) {
    const thisBooking = this;
    let hourNumber = utils.hourToNumber(hour);
    /*
    console.log({
      date: date,
      hourNumber: hourNumber,
      table: table,
      duration: duration,
      condition: hourNumber + duration,
      // counter: hourNumber + 0.5,
    });
    */
    for ( let hour = hourNumber; hour < hourNumber + duration; hour += 0.5) {
      if ( !thisBooking.booked.hasOwnProperty(date) ) {
        thisBooking.booked[date] = {};
      }
      if ( typeof thisBooking.booked[date][hour] == 'undefined' ) {
        thisBooking.booked[date][hour] = [];
      }
      if ( thisBooking.booked[date][hour].indexOf(table) < 0 ) {
        thisBooking.booked[date][hour].push(table);
      }
    }
    // console.log( thisBooking.booked );
  }
  updateDOM(e){
    const thisBooking = this;

    thisBooking.date = thisBooking.datePicker.value;
    thisBooking.hour = utils.hourToNumber(thisBooking.hourPicker.value);

    if ((e && e.detail && (e.detail.widget instanceof HourPicker || e.detail.widget instanceof DatePicker )) || e === 'reinitialise') {
      thisBooking.toggleTableSelection();
    }

    // console.log(thisBooking.dom.tables);
    for ( let table of thisBooking.dom.tables ) {
      const tableID = parseInt(table.getAttribute(settings.booking.tableIdAttribute));
      // console.log(typeof tableID);
      if ( typeof thisBooking.booked[thisBooking.date] != 'undefined'
        && typeof thisBooking.booked[thisBooking.date][thisBooking.hour] != 'undefined'
        && thisBooking.booked[thisBooking.date][thisBooking.hour].indexOf(tableID) >= 0 ) {
        //Add active class
        table.classList.add(classNames.booking.tableBooked);
        // console.log(tableID);
      } else {
        // Remove active class
        table.classList.remove(classNames.booking.tableBooked);
      }
    }
  }
  toggleTableSelection( table ){
    const thisBooking = this;

    if (!table || !table.classList.contains(classNames.booking.tableBooked)) {
      for ( let table of thisBooking.dom.tables ) {
        thisBooking.selectedTable = null;
        table.classList.remove(classNames.booking.tableSelected);
      }
    }

    if ( table && !table.classList.contains(classNames.booking.tableBooked) ) {
      table.classList.add(classNames.booking.tableSelected);
      thisBooking.selectedTable = parseInt(table.getAttribute(settings.booking.tableIdAttribute));
    }
  }
  makeBooking(){
    const thisBooking = this;
    const url = settings.db.url + '/' + settings.db.booking;
    const formData = utils.serializeFormToObject(thisBooking.dom.form);
    const starters = formData.starter || [];

    if (!thisBooking.selectedTable) {
      alert('Please select the table first');
      return false;
    }

    const payload = {
      date: thisBooking.datePicker.value,
      hour: thisBooking.hourPicker.value,
      table: thisBooking.selectedTable,
      duration: thisBooking.hoursAmount.value,
      ppl: thisBooking.peopleAmount.value,
      starters: starters,
      phone: formData.phone,
      address: formData.address
    };

    const options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(payload),
    };

    fetch(url, options)
      .then(function (response) {
        return response.json();
      })
      .then(function(parsedResponse){
        // console.log('booking response: ', parsedResponse);
        thisBooking.makeBooked(parsedResponse.date, parsedResponse.hour, parsedResponse.table, parsedResponse.duration);
        thisBooking.updateDOM('reinitialise');
      });
  }
}
