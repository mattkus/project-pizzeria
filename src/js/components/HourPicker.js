/* global rangeSlider */
import {settings} from '../settings.js';
import BaseWidget from './BaseWidget.js';
import {select} from '../settings.js';
import utils from '../utils.js';

export default class HourPicker extends BaseWidget {
  constructor( wrapper ) {
    super(wrapper, settings.hours.open);
    const thisWidget = this;

    thisWidget.dom.input = thisWidget.dom.wrapper.querySelector(select.widgets.hourPicker.input);
    thisWidget.dom.output = thisWidget.dom.wrapper.querySelector(select.widgets.hourPicker.output);
    thisWidget.value = thisWidget.dom.input.value;
    thisWidget.initPlugin();
  }
  initPlugin(){
    const thisWidget = this;

    rangeSlider.create(thisWidget.dom.input);

    thisWidget.dom.input.addEventListener('input', function(){
      const element = this;
      thisWidget.value = element.value;
      // console.log( thisWidget.value );
    });
  }
  parseValue(newValue){
    return utils.numberToHour(newValue);
  }
  isValid() {
    return true;
  }
  renderValue(){
    const thisWidget = this;

    thisWidget.dom.output.innerHTML = thisWidget.value;
  }
}
