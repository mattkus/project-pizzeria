import Product from './components/Product.js';
import Cart from './components/Cart.js';
import {select, settings, classNames} from './settings.js';
import Booking from './components/Booking.js';

const app = {
  initData: function(){
    const thisApp = this;
    const url = settings.db.url + '/' + settings.db.product;
    thisApp.data = {};

    fetch(url)
      .then(function (rawResponse) {
        return rawResponse.json();
      })
      .then(function (parsedResponse) {
        // console.log('parsedResponse', parsedResponse);
        /* save parsedResponse as thisApp.data.products */
        thisApp.data.products = parsedResponse;
        /* execute thisApp.initMenu */
        thisApp.initMenu();
      });
    // console.log('thisApp.data: ', JSON.stringify(thisApp.data));
  },
  initMenu: function(){
    const thisApp = this;
    // console.log('thisApp.data:', thisApp.data);

    for ( let productData in thisApp.data.products ) {
      // console.log('productData', thisApp.data.products[productData]);
      new Product( thisApp.data.products[productData].id, thisApp.data.products[productData] );
    }
  },
  initCart: function(){
    const thisApp = this;
    const cartElement = document.querySelector( select.containerOf.cart );
    thisApp.cart = new Cart( cartElement );
    thisApp.productList = document.querySelector(select.containerOf.menu);
    thisApp. productList.addEventListener('add-to-cart', function(e){
      thisApp.cart.add(e.detail.product);
    });
  },
  initPages: function(){
    const thisApp = this;

    thisApp.pages = Array.from(document.querySelector(select.containerOf.pages).children);
    thisApp.navLinks = Array.from(document.querySelectorAll(select.nav.links));

    let pagesMatchingHash = [];
    if ( window.location.hash.length > 2) {
      const idFromHash = window.location.hash.replace('#/','');
      pagesMatchingHash = thisApp.pages.filter( page => page.id == idFromHash );
    }
    thisApp.activatePage( pagesMatchingHash.length ? pagesMatchingHash[0].id : thisApp.pages[0].id );

    for ( let link of thisApp.navLinks ) {
      link.addEventListener('click', function(e){
        const clickedElement = this;
        e.preventDefault();

        /* TODO: get page id from href */
        const pageId = clickedElement.getAttribute('href').replace('#','');

        /* TODO: activate page */
        thisApp.activatePage(pageId);

      });
    }
  },
  activatePage: function( pageId ){
    const thisApp = this;
    window.location.hash = '#/' + pageId;

    for ( let link of thisApp.navLinks ) {
      link.classList.toggle(classNames.nav.active, link.getAttribute('href') == '#' + pageId);
    }
    for ( let page of thisApp.pages ) {
      page.classList.toggle(classNames.pages.active, page.id == pageId);
    }
  },
  initBooking: function(){
    const thisApp = this;
    const bookingWrapper = document.querySelector(select.containerOf.booking);
    thisApp.bookingWidget = new Booking(bookingWrapper);
  },
  init: function(){
    const thisApp = this;
    thisApp.initPages();
    thisApp.initData();
    thisApp.initCart();
    thisApp.initBooking();

    // console.log('*** App starting ***');
    // console.log('thisApp:', thisApp);
    // console.log('classNames:', classNames);
    // console.log('settings:', settings);
    // console.log('templates:', templates);
  },
};

app.init();